#ifndef __TICKER_H__
#define __TICKER_H__

#include <cstdint>
#include <functional>
#include <chrono>
#include <thread>
#include <future>
//#include <condition_variable>
//#include <iostream>
#include <mutex>

class Ticker {
public:
/* TYPES: */
	// Tick interval type in nanoseconds
	typedef std::chrono::nanoseconds tick_interval_type;	// TODO: MS chrono bug! Compile in XCode with double.
	// OnTick callback type
    typedef std::function<void()> on_tick_type;

/* CONSTANTS: */
	// Default ticker interval equal to 120 BPM
	static const tick_interval_type kDefaultTickerInterval;

/* INIT: */
	// Constructs object
    explicit Ticker (tick_interval_type tickInterval) 
	: _onTick ()
    , _tickInterval (tickInterval)
    , _running (false) {}
	// Destructs object
    ~Ticker () {}

/* PROPERTIES: */
	// Sets tick interval to a new value
    void setTickInterval (tick_interval_type tickInterval) {
        _tickIntervalMutex.lock();
        _tickInterval = tickInterval;
        _tickIntervalMutex.unlock();
    }
	// Sets on-tick callback to a new value
	void setOnTick (on_tick_type onTick) { _onTick = onTick; }

/* PUBLIC METHODS: */
	// Starts the ticker
    void start () {
        if (_running) return;
        _running = true;
        std::thread run( &Ticker::loop, this );
        run.detach();
    }
	// Stops the ticker
    void stop () { _running = false; }

private:
/* FIELDS: */
	// OnTick callback, called every tick
    on_tick_type			_onTick;
	// Tick interval between every tick
    tick_interval_type		_tickInterval;
	// Ticker running flag
    volatile bool			_running;
	// Tick interval mutex for thread safety
    std::mutex				_tickIntervalMutex;

/* PRIVATE METHODS: */
	// Inner loop executed on a separate thread to produce ticks
    void loop () {
		tick_interval_type ti;
        while (_running) {
            std::thread run (_onTick);
            run.detach();

            _tickIntervalMutex.lock();
            ti = _tickInterval;
            _tickIntervalMutex.unlock();

            std::this_thread::sleep_for( ti );
        }
    }
};

// Default ticker interval equal to 120 BPM
const Ticker::tick_interval_type Ticker::kDefaultTickerInterval (625000000UL / 120UL);

#endif // __TICKER_H__