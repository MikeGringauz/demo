#ifndef __BEAT_CLOCK_H__
#define __BEAT_CLOCK_H__

#include <cstdint>
#include "SongPosition.h"
#include "Ticker.h"

class BeatClock {
public:
/* INNER CLASSES: */
	// Inner state of beat clock
	enum class State : uint8_t {
		kStopped,
		kRunning,
		kPaused
	};

/* CONSTANTS: */
	// Clock pre-start delay to give a slave time to prepare (1ms)
	static const std::chrono::milliseconds kPreStartDelay;

/* CONSTRUCTORS/DISTRUCTORS: */
	// Constructs object
	BeatClock () 
	: _bpm (120UL)
	, _ticker (Ticker::kDefaultTickerInterval)
	, _songpos ()
	, _precount () {
		_ticker.setOnTick( [&] {
			//_tickerMutex.lock();
			_songpos.incrementTicks();
			if (_songpos.ticks() == 96U) {		// next beat...
				_songpos.resetTicks();
				_songpos.incrementBeats();
				if (_songpos.beats() == 4U) {	// next measure...
					_songpos.resetBeats();
					_songpos.incrementMeasures();
				}
			}
			if (_precount == 0U) {
				_precount = 4U;
				printf( "Send - RealTimeClock: %2i | %1i | %05i\n", 
					_songpos.ticks(), _songpos.beats(), _songpos.measures() );
			}
			--_precount;
			//_tickerMutex.unlock();
		} );
		_state = State::kStopped;
	}
	// Destructs object
	~BeatClock () {}

/* PROPERTIES: */
	// Gets current BPM value 
	uint64_t bpm () const { return _bpm; }
	// Sets current BPM to a new value
	void setBpm (uint64_t bpm) { 
		_bpm = bpm;
		Ticker::tick_interval_type span (625000000UL / _bpm);
		_ticker.setTickInterval( span );
	}

/* PUBLIC METHODS: */
	// Starts if currently stopped / Pauses if currently running / Resumes if currently paused
	void start () {
		switch (_state)
		{
		case State::kStopped:	// start...
			_songpos.reset();
			this->sendMeter();
			printf( "Send - RealTimeStart\n" );
			std::this_thread::sleep_for( kPreStartDelay );
			_state = State::kRunning;
			_ticker.start();
			break;
		case State::kRunning:	// pause...
			_ticker.stop();
			_state = State::kPaused;
			break;
		case State::kPaused:	// continue...
			printf( "Send - RealTimeContinue\n" );
			_state = State::kRunning;
			_ticker.start();
			break;
		}
	}
	// Stops if currently running or paused / Resets if currently stopped
	void stop () {
		switch (_state)
		{
		case State::kStopped:	// reset...
			_songpos.reset();
			this->sendMeter();
			printf( "Send - RealTimeStop\n" );
			break;
		case State::kRunning:
		case State::kPaused:	// stop...
			printf( "Send - RealTimeStop\n" );
			_ticker.stop();
			_precount = 0U;
			_state = State::kStopped;
			break;
		}
	}
	// Increments measure and resets sub-counters
	void forward () {
		if (_state == State::kRunning) _ticker.stop();
		_songpos.resetTicks();
		_songpos.resetBeats();
		_songpos.incrementMeasures();
		_precount = 0U;
		this->sendMeter();
		if (_state == State::kRunning) _ticker.start();
	}
	// Decrements measure and resets sub-counters
	void rewind () {
		if (_state == State::kRunning) _ticker.stop();
		_songpos.resetTicks();
		_songpos.resetBeats();
		if (_songpos.measures() > 0U) _songpos.decrementMeasures();
		_precount = 0U;
		this->sendMeter();
		if (_state == State::kRunning) _ticker.start();
	}

private:
/* FIELDS: */
	// Current BPM value
	uint64_t		_bpm;
	// Used for generating tick events.
	Ticker			_ticker;
	// Current song position
	SongPosition	_songpos;
	// Clock precount to scale from 96 to 24 ppqn (0..3)
	uint8_t			_precount;
	// Transport state  
	State			_state;
	// Ticker mutex for thread safety
	//std::mutex		_tickerMutex;

/* PRIVATE METHODS: */
	// Sends Song-Position MIDI message
	void sendMeter () {
		printf( "Send - SongPosition: %i\n", _songpos.meter() );
	}
};

// Clock pre-start delay to give a slave time to prepare (1ms)
const std::chrono::milliseconds BeatClock::kPreStartDelay (1);

#endif // __BEAT_CLOCK_H__