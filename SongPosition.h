#ifndef __SONG_POSITION_H__
#define __SONG_POSITION_H__

#include <cstdint>

class SongPosition {
public:
/* INIT: */
	// Constructs song position instance: Ticks | Beats | Measures
	SongPosition (uint8_t ticks = 0U, uint8_t beats = 0U, uint16_t measures = 0U) {
		_ticks = ticks;
		_beats = beats;
		_measures = measures;
	}
	~SongPosition () {}

/* PROPERTIES:: */
	// Counts from 0..95
	uint8_t ticks () const { return _ticks; }
	// Counts the quarter notes 0..3
	uint8_t beats () const { return _beats; }
	// Counts the measures 0..65535
	uint16_t measures () const { return _measures; }
	// Current meter location, sixteens-notes counter
	uint16_t meter () const { return (_beats << 2) | (_measures << 4); }

/* METHODS: */
	// Resets meter counters
	void reset () {
		_ticks = 0U;
		_beats = 0U;
		_measures = 0U;
	}
	// Resets ticks counter
	void resetTicks () { _ticks = 0U; }
	// Resets beats counter
	void resetBeats () { _beats = 0U; }
	// Increments ticks counter
	void incrementTicks () { ++_ticks; }
	// Increments beats counter
	void incrementBeats () { ++_beats; }
	// Increments measures counter
	void incrementMeasures () { ++_measures; }
	// Decrements measures counter
	void decrementMeasures () { --_measures; }

	bool operator== (const SongPosition& other) const {
		if (_ticks == other.ticks() 
			&& _beats == other.beats() 
			&& _measures == other.measures())	return true;
		else return false;
	}
	bool operator!= (const SongPosition& other) const {
		return !(*this == other);
	}
	
private:
/* FIELDS: */
	// Counts from 0..95
	uint8_t		_ticks;
	// Counts the quarter notes 0..3
	uint8_t		_beats;
	// Counts the measures 0..65535
	uint16_t	_measures;
};

inline bool operator<  (const SongPosition& lhs, const SongPosition& rhs) {
	if (lhs.measures() < rhs.measures()) return true;
	else if (lhs.measures() == rhs.measures()) {
		if (lhs.beats() < rhs.beats()) return true;
		else if (lhs.beats() == rhs.beats()) {
			if (lhs.ticks() < rhs.ticks()) return true;
			else return false;
		}
		else return false;
	}
	else return false;
}
inline bool operator>  (const SongPosition& lhs, const SongPosition& rhs) {
	return rhs < lhs;
}
inline bool operator<= (const SongPosition& lhs, const SongPosition& rhs) {
	return !(lhs > rhs);
}
inline bool operator>= (const SongPosition& lhs, const SongPosition& rhs) {
	return !(lhs < rhs);
}

#endif // __SONG_POSITION_H__